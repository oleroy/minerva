---
authors:
- admin
categories: []
date: "2021-05-06T00:00:00Z"
draft: false
featured: false
gallery_item:
- album: gallery
  caption: Default
  image: theme-default.png
- album: gallery
  caption: Ocean
  image: theme-ocean.png
- album: gallery
  caption: Forest
  image: theme-forest.png
- album: gallery
  caption: Dark
  image: theme-dark.png
- album: gallery
  caption: Apogee
  image: theme-apogee.png
- album: gallery
  caption: 1950s
  image: theme-1950s.png
- album: gallery
  caption: Coffee theme with Playfair font
  image: theme-coffee-playfair.png
- album: gallery
  caption: Strawberry
  image: theme-strawberry.png
image:
  caption: "Qgis2threejs"
  focal_point: "smart"
  preview_only: false
projects: []
subtitle: ""
summary: Installing a QGIS plugin
tags:
  - QGIS
  - GIS
title: Installing Qgis2threejs!
---

For one of our projects we need more than "vanilla" QGIS. So we will have to install and load a **plugin**. 

**Plugins** are third party software that increase QGIS functionality. 

# Using the Plugins Management window

If you go into the `Plugins`'s menu then pick `Manage and Install Plugins` you will open up a window : 

![](qgis_treejs.png)

You can then search for the name of the plugin you want to add, here we will [Qgis2threejs](https://qgis2threejs.readthedocs.io/en/docs/).Then you click on `Install Plugin`. 

Yes, the most difficult part of adding a plugin is to know which one you need. For this exercise it wasn't hard because *Qgis2threejs* is a famous plugin for 3D visualization. But if you want to do something and it looks like QGIS is not enough try asking your favorite internet browser or check forums and stack exchange. 
 

# Check where your plugin will be !
 
 Qgis2threejs is on the `web` menu. Usually plugins will also add an icon in your `Plugins Toolbar` :
 
 Like this one : 
 
 ![](Qgisthreejs.png)