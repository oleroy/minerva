---
authors:
- admin
categories: []
date: "2021-05-06T00:00:00Z"
draft: false
featured: false
gallery_item:
- album: gallery
  caption: Default
  image: theme-default.png
- album: gallery
  caption: Ocean
  image: theme-ocean.png
- album: gallery
  caption: Forest
  image: theme-forest.png
- album: gallery
  caption: Dark
  image: theme-dark.png
- album: gallery
  caption: Apogee
  image: theme-apogee.png
- album: gallery
  caption: 1950s
  image: theme-1950s.png
- album: gallery
  caption: Coffee theme with Playfair font
  image: theme-coffee-playfair.png
- album: gallery
  caption: Strawberry
  image: theme-strawberry.png
image:
  caption: 
  focal_point: ""
  preview_only: false
projects: []
subtitle: How to work with shapefile
summary: How to work with shapefile
tags:
  - Vector
  - GIS
title: Shapefile format make it simple!
---

# We love .shp!

shapefile is one of the oldest **vector** (TODO link to description) format and it's widely use. It is developed by [ESRI](https://www.esri.com/en-us/home) but is now supported (read/write) by a wide of GIS software.

Unlike it's name sound like shapefile consist of a collection of files, some are mandatory and other can be added. Those files share the same name but with different filename extensions. We will provide you an example. 

## Mandatory files / Other files

### Mandatory file

Let's say a shapefile `example` : it must have 3 files : 

- `example.shp`: the shape format, contains geometry features

- `example.shx`:  an index for the feature geometry to allow a quicker access

- `example.dbf`: a way to store attributes of each shape

When you create a shapefile this 3 files will be created in your hard drive and if you want to share it you need at least this 3 files, just providing the `*.shp` will not be enough! 

### Other files

One other important, but not mandatory, file will be `example.prj` this will be a plain text file that contain the projection associated with the shapefile. Other associated files can be used to create more indexing (a way to optimize queries in the features) or provides metadatas. 

## Some limitations to keep in mind 

- Shapefile does not handle topological information it is part of the **spaghetti data model** family (TODO link to topological data model). If you move one of your spaghetti it will not affect an other one, spaghetti are kind of independent of each other. That also means if you are representing, for example, a border between two countries moving one border or country will not move the next door country and its border you will have to move both, and try to make them match well.

- Shapefile are also limited in storage and will not be the best option for complicated queries. 

- Don't try using some kind of Unicode (even accent) in their name or in their field name, the format will not be able to understand it. Also on the field name restriction : they must be less than 10 characters (yes, keep it simple).

- Finaly you should only store one shape type (points, polyline, polygon) per shapefile 


