---
authors:
- admin
categories: []
date: "2021-05-06T00:00:00Z"
draft: false
featured: false
gallery_item:
- album: gallery
  caption: Default
  image: theme-default.png
- album: gallery
  caption: Ocean
  image: theme-ocean.png
- album: gallery
  caption: Forest
  image: theme-forest.png
- album: gallery
  caption: Dark
  image: theme-dark.png
- album: gallery
  caption: Apogee
  image: theme-apogee.png
- album: gallery
  caption: 1950s
  image: theme-1950s.png
- album: gallery
  caption: Coffee theme with Playfair font
  image: theme-coffee-playfair.png
- album: gallery
  caption: Strawberry
  image: theme-strawberry.png
image:
  caption: 
  focal_point: "smart"
  preview_only: false
projects: []
subtitle: ""
summary: A simple way to stay organized
tags:
  - QGIS
  - GIS
  - Data Management
title: Start with a QGIS project!
---


# Stay organized!

Dealing with data can become a bit messy. It is good to follow some structures/guidelines to make it easier. 

We will provide you with guidelines, they are not rules, you can break them, adapt them and make them yours but you should keep it mind that staying consistent with them will save you a lot of time.  

First very important advice is that you should keep track of what you are doing. Where do I download this data ? Which processing have I done to obtain something ? Why did I use these parameters ? etc. You have plenty of ways of doing it. Taking notes in a paper notebook is a very common and good way. You can also do it in digital formats:  simple word document, online tools, wikis. Like in paper, it's important to keep some kind of structure in it. By default it is good to, at least, add the date to keep an idea of the chronology.

In this small simple example we have added a `my_notepad.doc` for that purpose. As you can see it is in a "project repository" so we have decided to keep a file for every project but if you manage multiple projects another option is to keep your note in one central system. 


## Repositories structures

Below we have put together a small example on how to organize your GIS project. First you have a `/Data` repository that will contain all of your data. It is divided into `raw` data and `transformed` data. The first one is where you put your data before any kind of processing and the second one is where you store your results or intermediaries steps. Another common layer to be added is that you can divide those directories with a `Raster` and `Vector` sub directories. 

If your raw data are voluminous and shared between multiple projects they can have their own directory or even better stored in a database.  

Another common directory that we don't have include here is a temporary one. You can either use the temporary folder of your operating system or create one in your project. It is useful if you are planning to do a lot of tests and produce data that will not be keeped.  

![](featured.png)

Then we have a directory with all of our word, pdf document like some scientific paper or our notepad. 

It is also good to have a directory where you can store your visualization.  It can be maps (in a `Map`)  or statistical plots (`Viz`).

Finally, you will find at the root of your project's directory a file with an extension `.qgz`. It doesn't need to be at the same place as your data and visualizations but it make it a bit easier. 


## Saving a QGIS project

So what is this `my_cool_project.qgz` ? 

When you are working in QGIS you are adding layers, changing default variables, maybe picking some specific symbologies etc.. After you close QGIS, if you don't save it, all the settings in your work environment will be lost. A QGIS project will store all of this information so when you choose to reopen it you will find it in the same state!

For that you just need to go to the `project` menu, select `Save As..` pick a name (better than `my_cool_project.qgz`) and a good location. If you make some changes and want to update then you just need to save them. Keep it mind that you save data for example when you are editing it at the layer level, if you have unsaved data and want to save the project it will not save it.

You can explore all the options available in your project by going into `projet`menu and `Properties`:

![](QGIS_project.png)

The QGIS project is saving related links and variables it will not save or contains your data so if you want to give access to your whole project you will need to copy everything (or maybe use geopackage)!

