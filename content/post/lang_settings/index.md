---
authors:
- admin
categories: []
date: "2021-06-08T00:00:00Z"
draft: false
featured: false
gallery_item:
- album: gallery
  caption: Default
  image: theme-default.png
- album: gallery
  caption: Ocean
  image: theme-ocean.png
- album: gallery
  caption: Forest
  image: theme-forest.png
- album: gallery
  caption: Dark
  image: theme-dark.png
- album: gallery
  caption: Apogee
  image: theme-apogee.png
- album: gallery
  caption: 1950s
  image: theme-1950s.png
- album: gallery
  caption: Coffee theme with Playfair font
  image: theme-coffee-playfair.png
- album: gallery
  caption: Strawberry
  image: theme-strawberry.png
image:
  caption: 
  preview_only: false
  focal_point: "Smart"
projects: []
subtitle: 
summary: Setting language in QGIS's UI
  - QGIS
title: Setting language in QGIS's UI
---


# Language by default 

By default QGIS will use your computer local setting for the User Interface's language (UI) and other countries related matters (date format, decimal separator, etc..).

If you want another one this can be defined.

#  Setting your UI language

You will need to:

1. Go in the `Setting Menu` -> `option` -> `General` Tab and  set up `Override System Locale"


![](setting_language.gif)

2. Restart QGIS

# Minerva Project Languages :eu:!

## :fr: France:

![](france_setting.png)

## :greece: Greece: 

![](greece_setting.png)

## :it: Italy:

![](italy_setting.png)

## :serbia: Serbia:

Sadly no support...


## :slovenia: Slovenia:

Sadly no support...

##  :es: Spain:

![](spain_setting.png)
