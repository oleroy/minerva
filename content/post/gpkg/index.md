---
authors:
- admin
categories: []
date: "2021-05-06T00:00:00Z"
draft: false
featured: false
gallery_item:
- album: gallery
  caption: Default
  image: theme-default.png
- album: gallery
  caption: Ocean
  image: theme-ocean.png
- album: gallery
  caption: Forest
  image: theme-forest.png
- album: gallery
  caption: Dark
  image: theme-dark.png
- album: gallery
  caption: Apogee
  image: theme-apogee.png
- album: gallery
  caption: 1950s
  image: theme-1950s.png
- album: gallery
  caption: Coffee theme with Playfair font
  image: theme-coffee-playfair.png
- album: gallery
  caption: Strawberry
  image: theme-strawberry.png
image:
  preview_only: true
  caption: 
  focal_point: "smart"
  preview_only: false
projects: []
subtitle: 
summary: Learn more about one of the OGC data encoding format
tags:
  - Vector
  - GIS
  - Raster
  - Data Management
title: GeoPackage an OGC Standard!
---

# An OGC's data encoding format

As awesome [shapefile](https://oleroy.gitpages.huma-num.fr/minerva/post/shapefile/) format is it is still has some limitations. One of them is that it has the tendency (*euphemism*) to need "sidecar files", i.e. relies on added files to increase its functionality, which can complicate a lot of stuff. It is also limited in what it can store. 

For all of the above reasons (and more!) creating a new data encoding format was needed. Also, as the spatial industry expanded, the need to share agreement on requirements and recommendations (a.k.a a "standard") for data encoding increased. Interoperability helps everyone focus on other stuff than reinventing the wheels or producing another new encoding format.

![XKCD](https://imgs.xkcd.com/comics/standards.png)

The [OGC](https://www.ogc.org/) is a standards organization providing specifications and produces standards for the geospatial world. They are organizing their members to share specifications and good practices (and avoiding the situations where we have one more competing standard). 


## What is this standard ?

Well you can learn it in detail [here](https://opengeospatial.github.io/e-learning/geopackage/text/basic-index.html) but we will provide you with a summary.  

GeoPackage (or gpkg) uses:

- SQLite relational database to store:

    * vector features data
    * Imagery tile matrix sets
    * Raster map matrix sets
    * Non spatial tabular data 
    * Metadata
    

- Store geometry in [WKT](https://en.wikipedia.org/wiki/Well-known_text_representation_of_geometry) following an other OGC standard ([Simple Features](https://en.wikipedia.org/wiki/Simple_Features)) 

## How can I use it ?

GeoPackage can be opened, displayed and edited with QGIS or with other SQLite's tools. Like every SQL database it organizes and stores information in tables so it is easy to query (SQL stands for Structured Query Language). Some tables are mandatory, you must have them, and others are optional or related to the content of the GeoPackage. As indicated above, vector are stored in *features* (i.e. a things you can represent with a geometry) and raster as a *tiles* (adjacent images to keep it simple). 

## But why should I use .shp now ?

GeoPackage makes sure you will have everything (or nearly) for your GIS needs so why should you use shapefile for vector data ?

Well it depends on your needs! Shapefile has been around for more than 20 years so it is well known and some people still have trouble dealing with `.gpkg`. Our advice will be to use what other people in your working place use except when you require some specific functionality. 

