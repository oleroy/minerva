---
authors:
- admin
categories: []
date: "2021-06-08T00:00:00Z"
draft: false
featured: false
gallery_item:
- album: gallery
  caption: Default
  image: theme-default.png
- album: gallery
  caption: Ocean
  image: theme-ocean.png
- album: gallery
  caption: Forest
  image: theme-forest.png
- album: gallery
  caption: Dark
  image: theme-dark.png
- album: gallery
  caption: Apogee
  image: theme-apogee.png
- album: gallery
  caption: 1950s
  image: theme-1950s.png
- album: gallery
  caption: Coffee theme with Playfair font
  image: theme-coffee-playfair.png
- album: gallery
  caption: Strawberry
  image: theme-strawberry.png
image:
  caption: "Comic by [xkcd](https://xkcd.com/1179/)"
  preview_only: false
projects: []
subtitle: 
summary: World is huge, and we have a lot of ways to store date formats.
tags:
  - Data Management
  - GIS
title: Date format around the world!
---

# Date formats


As explain in [xkcd explain](https://www.explainxkcd.com/wiki/index.php/1179:_ISO_8601) you have an huge of way on how to write date (year, month, day) around the world.

Stay consistent!