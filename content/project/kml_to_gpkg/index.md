---
date: "2021-07-12T00:00:00Z"
external_link: null
image:
  caption: ""
  focal_point: Smart
summary: "Converting KML to GeoPackage"
tags:
- Vector
- Data Management
- Visoka region
title: "Moving from KML to GPKG!"
---

In this project, historians have localized villages and stored them into KML files. Every village of one region is stored in KML files. 

The name of the regions are:

* Visok
* Vidin
* Znepolje
* Deserted Villages (not a region here)

It is a bit better to store them in one file and add a new field "region" that will take the name of each region. We will first convert the KML into a unique GeoPackage then merge the various layers into one. 

You can find more information about GeoPackage [here](https://oleroy.gitpages.huma-num.fr/minerva/post/gpkg/). 

# Converting KML to a .gpkg

First, you will have to add all the KML to the layer's panel. Then you have at least two options. The first one is right click on one of the layers, go with `Export` and use the `Save as..` option. A new windows will be prompted:

![](save_vectir_layer.png)

As you can see `Format` is in GeoPackage. I saved it in `villges.gpkg`. A GeoPackage can have multiple layers, I started with Visok, so I picked this name for the first layer. I also picked the same CRS from my project.  

The second option is, selecting the layer, going into the `Layer menu` and using `Save as..` and the same window will be displayed.

Now that I have a .gpkg I will add to it every village from the other regions: save them into  `villages.gpkg` as a new layer and with the same CRS. You should have something like that: 

![](result_save_as.png)

# Merging the layers 

All of this new layers have the same points geometry, same CRS, carry the same fields (i.e. their columns) so they should be in only one layer regrouping (`merge`) all of them but with a new field helping us distinguish them.

We are now in a good spot to merge them! We are adding, into a new table, all the rows of each previous table by creating a new field that contains previous layers' names.

For that we go to the  `Vector`'s menu, pick  `Data Management tools` and select `Merge Vector Layer`. First we will need to indicate which layers should be merged by clicking on the [...]  and selecting them:

![](selecting_layer_to_merge.png)

After, you need to go back into the "Input Layers" (using the blue arrow), select the CRS and if needed save the new file (here I went with a non original `villages_merged.gpkg`).

![](merge_vector_imput.png)

Then `Run`!

After adjusting the symbology you should have something like that: 

![](villages_gpkg.png)


# A bonus!

This is already good but as a bonus let's see if we can remove the unnecessary "villages" in front of each name of regions. 

For that we will go in the field calculator:

![](mActionCalculateField.png)

We will then subtract everything after "villages ". To do that we need to know the length of every string (as an example "villages Vidin" contains 14 characters) and the length of "villages " (9 counting the white space). 

Then in the field calculator we will create a new fields "region", it will have string as type and the expression to produce it will be :

```Python
substr("layer".
    9,
    length("layer")
    )
```

It looks a bit difficult because we are using two functions (`substr()` and `length()`) at the same time. The first one uses a field (here "layer") where we will subtract from the 9nth character to the end of the length of each value in the field "layer" (this is length("layer")).  

![](fieldcalculator.png)

TODO redo it to remove the popup ...