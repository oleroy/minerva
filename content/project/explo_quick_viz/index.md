---
date: "2021-08-31T00:00:00Z"
external_link: null
image:
  caption: ""
  focal_point: Smart
summary: "Exploring Data with Quick visualization"
tags:
- Data Analysis
- Data Management
- Seismic Hazard
title: "Exploring our Data"
---


We have [downloaded](https://oleroy.gitpages.huma-num.fr/minerva/courses/seismic_hazard/) the *Register of Immovable Cultural Heritage* and **The map of Seismic Hazard – Design ground acceleration**. First let's see if we can explore a bit visually the data.   

## A Better Visualization

The power of QGIS also lies in its capacity to display a lot of data. 

Just after adding the two layers you should have something like this:

![](raw_data.png)

The color may differ because QGIS picks them randomly.

A lot can already be said about it but this display is very basic. Let's learn how we can improve it!

### Rename my layers

First we will rename our layers. For that you need to right click on each layer and select `Rename Layer`, then you can edit the name. This is what I get now:

![](rename_layers.png)

Using `Rename Layer` we are **not** renaming the file(s) behind this layer, only the name displaying. 

### Changing Symbology 

To change the `Symbology` of a layer we right click on it and select `Properties...` and pick `Symbology`:

![](Layer_Properties.png)

We have 2 layers: one is representing Cultural Heritage Sites as points and the other is representing Seismic Hazard in different areas (polygons). Cultural Heritage sites can be of different types meaning this column is categorical (*Discrete*) and Seismic hazard is represented as ground acceleration in *g* as a *continuous* variable. Our current representation doesn't reflect that.

#### Changing Seismic Hazard Symbology:

 Even if we have a continuous variable in the Seismic hazard layer we have few different polygons so we elected a graduated symbology. For that, in `Symbology` we picked `Graduated` instead of `Single Symbol`, then chose the `POSPESEK` field. We need to pick the number of classes to divide this continuous variable, here 5 is good (so no change from default setting) then we classify them. For that we used the `equal count` method. This method tries to put the same number of polygons in every class using quantiles. The algorithm order the value of `POSPESEK`, then divide it in 5 part:

`0.100+ | 0.100+ | 0.125 | 0.125 | 0.125 ] 0.149+ | 0.149+ | 0.149+ ] 0.174+ | 0.200+ | 0.200+ ] 0.225 | 0.225 | 0.225 ] 0.25 | 0.25]`

We have 16 values: around 3 values per class. Here, the third value is the same value as the fourth and fifth  (0.125) and it was decided to put the tie in the same classes. Then we go for 0.149+ (same issue). Next quantile is 3.2 times 3: closer to 0.200+. The other gives us 0.225 and so our last class contains only 2 polygons. 4 classes will not have been better in this aspect (try it!).

If needed you can also specify each class breaks by hand (double click on a class).

Don't forget to `Apply`!

![](graduate.gif)

#### Displaying various Types for Cultural Heritage sites:

*Zvrst* in Slovenian means "Type". This field records the type of cultural heritage. We can explore it by going into `Layer Property` and then `Symbology`. Here we will change `Single Symbol` to `Categorized`, select `Zvrst` for `Value` then use `Classify`. I also uncheck `all other values`

![](categozied.png)

We can change the colors and the symbol if needed but I like doing that a bit later (mostly because we have planned to only keep buildings, so no need to spend time on data that we will not keep). It will help us to change `Legend` with English translation. The table below will help you doing it:

<center>

| Slovenian                   | English                         |
|-----------------------------|---------------------------------|
| arheološka najdišča         | archaeological sites            |
| drugi objekti in naprave    | other facilities and devices    |
| kulturna krajina            | cultural landscape              |
| naselja in njihovi deli     | settlements and their parts     |
| ostalo                      | other                           |
| parki in vrtovi             | parks and gardens               |
| spominski objekti in kraji  | memorials and places            |
| stavbe                      | buildings                       |
| stavbe s parki ali z vrtovi | buildings with parks or gardens |

</center>

You will have something like this: 

![](legend_change.png)

And after the layer is displaying like this:

![](Layers_english.png)

This is great because we have not make any kind of modifications inside the data, the column type is still in Slovenian. 

We can still improve our exploration a bit by displaying the number of every type. For that we right click on the layer and check `Show Feature count`

![](show_feature_count.gif)

[Next](https://oleroy.gitpages.huma-num.fr/minerva/project/selection_filter/) we will see how to filter data!
