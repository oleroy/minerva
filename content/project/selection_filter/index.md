---
date: "2021-08-31T00:00:00Z"
external_link: null
image:
  caption: "https://commons.wikimedia.org/wiki/User:RRZEicons"
  focal_point: Smart
summary: "Filtering data"
tags:
- Data Analysis
- Data Management
- Seismic Hazard
title: "Filtering Data of Interest"
---

After [exploring](https://oleroy.gitpages.huma-num.fr/minerva/project/explo_quick_viz/) a bit of our data set and discovering that it contains all kinds of cultural heritage sites (buildings, landscape, etc) for our project we are more interested in the buildings. How can we select them and only them ?

## Filtering for our needs:

Now let's filter and keep only the cultural heritage site with buildings. For that we need to open the attribute table (right click on the layer -> `Open Attribute Table`).

Then we press the filter button:

![](mActionFilterMap.png)

The Attribute Table of the layer of the **Register of Immovable Cultural Heritage**  will go in the view form (to go back to `table view` use the table action button on the bottom right side) and we can filter the filter *Zvrst* with *stavbe*.

![](filter_form.png)

We can then click on `Filter Features`. 

![](filtered.png)

As we can see on the header of this window we have now 1349 features filtered. This number is for the 1299 `buildings` plus the 50 `buildings with parks or gardens`. If we wanted to just have the buildings we could have changed `Contains` to `Equal to (=)`. 

![](equal_to.png)

## Selecting features

When we feel this filter is good we can use the `Select Features` menu and press it. This will highlight (in yellow by default in QGIS) all the features filtered and selected. 

![](filtered_selected.png)

We can then save our selection in a new file (a shapefile for example). QGIS also allows you to save it in a temporary file (if the file is not saved it will not be conserved for later use). This option can be good when you are trying various options (prototyping) but keep in mind that you can “lose it”.

![](export_slection.gif)
