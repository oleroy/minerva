---
date: "2021-07-12T00:00:00Z"
external_link: null
image:
  caption: ""
  focal_point: Smart
summary: "Display a 3D"
tags:
- Data Visualization
- Visoka region
title: "3D view of Visoka region"
---

The goal here is to produce a 3D web visualization of the surrounding of the Visoka region. You can learn more about the plugin allowing us to do it [here](https://qgis2threejs.readthedocs.io/en/docs/).  

## What do we need ?

0. It is easier to work in a [project](https://oleroy.gitpages.huma-num.fr/minerva/post/qgis_project/) 
1. All the [villages](https://oleroy.gitpages.huma-num.fr/minerva/project/kml_to_gpkg/) 
2. The [DEM](https://oleroy.gitpages.huma-num.fr/minerva/project/prepare_dem/) of this area
3. Qgis2threejs plugin [installed](https://oleroy.gitpages.huma-num.fr/minerva/post/plugins_threejs/)

Your layers panel should look like that:

![](starting_visoka.png)

## Better color ramp for our DEM

First let's pick a better color ramp for our DEM. For that you will need to right click on the DEM layer and select the `Symbology tab`. Then change here settings in `Band Rendering`:

  1.  Change `Render Type` to `Paletted/Unique Value` 
  2.  No change on `Band`, we just have one
  3.  In `Color Ramp` use the down arrow at the end and pick `All Color Ramp`
  4.  Here we picked **BrBG**
  5.  We clicked on the color ramp and `Invert Color Ramp`
  6.  Then we used `Classify` to map every value from our raster to a color value in our color ramp
  7.  And finally we click on `Apply` and `Ok`!

![](color_ramp.gif)

Potential amelioration: our highest points are in the south east, if we remove (see crop/clip) them we can have a less stretched palette and maybe a better visualization.   

## Qgis2threejs Exporter

Let's load Qgisthreejs plugins:

![](qgis2threejs_exporter.png)

We have something and it can be improve with `Scene Settings` (using the `Scene` menu).

Here I just increase the `Vertical exaggeration` to `3.0` and apply the change. 

Then you go in File and `export to web`, I like the default version but feel free to change for the mobile version or the viewer with GUI (a bit heavier) if needed. 

![](export_to_web.png)

[Explore the result!](https://oleroy.gitpages.huma-num.fr/minerva/project/3d_view/view/index.html)
