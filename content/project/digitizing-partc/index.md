---
title: "Digitizing some data part-C!"
external_link: null
date: '2021-06-01T00:00:00Z'
summary: 'Make a vector from an historical map part C: Attribute table'
tags:
- Vector
- Digitizing
- Input Data
- Coal Basin Saint-Etienne
image:
  caption: null
  focal_point: Smart
bibliography: [tree.bib]
link-citations: true
---

The set up was done [here](https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing/) and [here](https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing-partb/)

We have 60 features representing mining concessions. To move forward we have to add to them the information from the legend. 

# Opening the Attribute Table

Each feature can be linked to **fields** describing them. A field can be represented in a **column** in a table and sometimes qualified as such. Each feature (here our mining concessions) will be represented in a **row**. 

You can access to this table by right-clicking the layer and select `Open Attribute Table`: 

![](open_attribute_table.gif)

The table looks like that after turning on the editing mode (yes another way to enter/leave the editing mode!): 

![](empty_attribute_table.png)

Pretty empty! Let's see how we can pour data in it!

# Adding New Fields

We will add new fields and attribute value to them. To do that we have to give them a name and provide them a **field type** (sometimes also named data type). The name can't be to big[^1] and must be explicit for you, you-in-few-months and others [see @Bromman_Woo_2018 for plenty of good advice]. The field type [TODO data type / Field type in QGIS] will give the computer and software instructions on how to save this information.   

[^1]: see [here](https://oleroy.gitpages.huma-num.fr/minerva/post/shapefile/).

## Planning Ahead

We have 5 kinds of information we want to transcribe. 

First, the number of mining concessions will be a great `id`. An identifier is a field with an unique value per feature. It is also known as a primary key.

Then we have the name of the mining concession. We will add it as a `Text` (or *string*) field type with the name of `mining_con`.

The capacities will be a `Whole number` (aka *integer*) with `cap_hect` as a name. 

The date of concessions will be a `Date` and named `date_con`.

Finally the color of each mining concessions is related to an owner and even if the map doesn't provide all of their names we at least know that each color matches one owner and this should be kept. We will create a field `owner` with a `Text` type. 

## Adding them!

Now that we have decided what kind of information and how we will represent and store it we just need to do it.

In the `Attribute table` let's click in the `New Fields` icon:

![](./attribute_table_icon/mActionNewAttribute.png)

We get this blank window:

![](add_field_blank.png)

After filling it, it will look more like that:

![](add_field_full.png)

`Name` and `Type` were already discussed. `Comment` is useful to help you remember weird names but are a "nice-to-have" but not a requirement. `Length` is more tricky if you leave it at 10 you will only be able to save a string with 10 or less characters (counting white space). After doing some quick count on the legend we fixed it at 50. 

After a few clicks we have created all the columns and are ready to edit!

![](attribute_table_new_field.png)

We prefer the `Table View` over the `Form View`: you can switch from one to another by clicking the `Table View` icons in the bottom right corner of the `Attribute Table`. 

![](./attribute_table_icon/mActionOpenTable.png)

After a bit of work we are done. 

![](editing_done.png)

Now we are ready to explore and represent this data set! 