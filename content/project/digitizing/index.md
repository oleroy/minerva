---
date: "2021-05-05T00:00:00Z"
external_link: null
image:
  caption: 
  focal_point: Smart
summary: "Make a vector from an historical map part-A: Before editing"
tags:
- Vector
- Digitizing
- Input Data
- Coal Basin Saint-Etienne
title: Digitizing Some Data part-A!
---

<script>
function confortable() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>

# Planning Ahead 

Now that we have our map[^1] with a correct CRS[^2] we can represent it on QGIS, use it as a background image, etc.. But what if we need to use information on it? For that, we will have to use another GIS format that allows QGIS and computers to process it. 

[^1]: It was done [here](https://oleroy.gitpages.huma-num.fr/minerva/project/georeferencing/)

[^2]: We are in a geographic coordinates reference system. We find that at this scale it is better and easier to go with projected one so we will change it [TODO link].

We will **digitize** this raster, which only uses pixels as a way of storing information, to a vector file format: a [shapefile](https://oleroy.gitpages.huma-num.fr/minerva/post/shapefile/).

Like every operation, jumping directly in QGIS is not always the best approach. The first step should be a planning one: what do you want to do with this data later? What kind of information will be needed and how to represent it for you, your coworkers and a computer? How much time can you dedicate to that? etc .. 

Writing a few lines on a paper or in a text document will help you a lot and can be reused later.

For this exercise we have already done that and made all the decisions for you. We will try to explain them as the workflow progresses but keep in mind that it was planned before and not just us "following the software". 


# Creating A New Shapefile

Shapefile is a file format that has a long history and is widely used. It has some limitations but they will not be a problem here. Other file formats or ways to store/access data could be done but in a trade off simplicity/benefit we picked simplicity. 

To create a new shapefile you can either go in the `layer` menu  -> `Create Layer` -> `New Shapefile Layer` or use the `New Shapefile Layer` icon:

![](new_shape_icon.png)

Then we get the `New Shapefile Layer` windows:

![](new_shapefile_layer.png)


We will have to :

- choose a name (TODO link to convention in naming)

- pick a file encoding standard

- select a geometry type

- add more dimensions

- use an EPSG


We will not insist too much here on name convention but remember : keep it simple and **consistant**. You should pick UTF-8 for file encoding, using your system's one is a bad idea because we don't know what other people are using or what you will use in few years. UTF-8 is the by default standard so, trust us, pick it.  

We want to represent a surface so we are selecting `polygon`. If you are unfamiliar or want a quick refresh on geometry types you can go here (TODO Link). We don't need to add more dimensions, Z is usually used for altitude and M for measuring precision.  

Finally, we picked the same *EPSG* of the project: 3857 [TODO change it for 2154]. 

We will not add a new field and keep that for later. 

## Before Start Editing: Snapping Options 

Shapefile format doesn't require to follow topological rules but following some (avoiding small gaps or overlaps between different polygons) will help us avoid trouble later. For that we will use some `Snapping Options`. 

They are options related to your QGIS project so you can find them under the `Project` menu -> `Snapping Options`. It will open the `Project Snapping Settings` windows. 

![](project_snapping_settings.png)

We find that it is easier to use the `Advanced Configuration` to get it toggle `all layers` to `Advanced Configuration`.  

![](advanced_conf.png)

- We didn't change the `Tolerance` of the snapping or `Units` associated. With this setup it means that when we are creating a new vertex QGIS will check if a vertex is close (to 12 px) to an already existing vertex. If yes it will create the vertex on top of it and if not it will create it where you clicked. 

- We also toggled on the `Topological Editing` on even if here it will probably be not needed (it is useful when you are moving boundaries).

- Then you used also `Self-snapping` (TODO look like it allow to handle better temporary editing but unsure and need to verify)

- We also added a segment to the `Type` of snapping object. 

- Finally we want to `Avoid Overlap`

If we don't select this option we will have some overlap:

![](polygon_without_overlap.gif)

With overlap it follow the boundary of the other polygon:

![](polygon_overlap.gif) 

Next part is [here](https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing-partb/).

