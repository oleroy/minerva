---
date: "2021-10-08T00:00:00Z"
external_link: null
image:
  caption: ""
  focal_point: Smart
summary: "See the viewshed of each tower and try to found undiscovered one"
tags:
- Data Analysis
- Data Management
- Tuscan Coast Towers
title: "Intervisibility between towers"
---


We have our towers and a DEM of the regions, now we can try an intervibility analysis between them. We will be able to determine what can be seen from a tower and maybe pinpoint a place where we are lacking towers and where research should be made to discover them! 

We will: 

- Download and use a plugins 
- Prepare the DEM
- Create viewpoints 
- Create viewsheds and a visibility graphs

## Download he plugins 

First we need to download the [visibility analysis](https://www.zoran-cuckovic.from.hr/QGIS-visibility-analysis/) plugins. Other options to create viewshed and visibility analysis exist (SAGA offers one) but we found that this plugin provided the best user experience and great results. It has also being the topic of a peer review article[^1].

[^1]: Cuckovic, (2016), Advanced viewshed analysis: a Quantum GIS plug-in for the analysis of visual landscapes, Journal of Open Source Software, 1(4), 32, doi:10.21105/joss.00032

To download it you can follow this [post](https://oleroy.gitpages.huma-num.fr/minerva/post/plugins_threejs/). and adapt it. When it is done you can find the plugins in the `toolbox`. 

See :

![](visbility_analysis.png)

## Loading and preprocessing data 

To start our analysis we have three layers: 

- OpenStreetMap layer, just to get a feeling of where we are

- The current know locations of some towers

- A DEM of the whole region

The DEM was provided by the [Istituto Superiore per la Protezione e la Ricerca Ambientale](https://www.isprambiente.gov.it/it/progetti/cartella-progetti-in-corso/suolo-e-territorio-1/cartografia-gravimetrica-digitale/i-dati-gravimetrici) and the geopackage of this coastal tower was provided by the [italian lab team].

![](start_inter.png)

We should start by cropping our DEM a bit. You can follow and adapt this [part](https://oleroy.gitpages.huma-num.fr/minerva/project/prepare_dem/) of the Serbian case study.  

To sum it up we have, create a bounding box (a simple polygon layer in shapefile) that we will use to crop the DEM:

![](bbox.png)

And after:

![](croped_edm.png)


## Create viewpoints

The first step of visibility analysis is creating vewpoints. Here it will be our towers. To do that we go in the `Processing Toolbox` and open the `create viewpoints` in the `Visibility analysis` section. 

The `Observer locations(s)` are the `Torri_costiere_32632` and the `Digital elevation model` is our `small_dem`. We are using feature ids so no need to change anything in the next options. We also stay with 5000 m in radius and want the all radius. We can use the `Altezza` field for both the observer height and target height. We then save them in a geopackage called `create_viewpoint.gpkg`.

![](viewpoints.png)

## Viewsheds

We have our viewpoints and we can proceed with producing the viewshed. For that we go into the `Analysis` section in `Visibility analysis`. We just have to set up the viewpoint and the DEM (here `Addition` is fine to use in `Combining mulitple output` but minimum could have been also good because we just want to know where we are lacking tower visibility). We also didn't add `earth curvature` because we are on a small area and didn't change `Atmospheric refraction`. We saved our results in `viewshed.tiff`. 

![](viewshed.png)

This is our results (after fiddling a bit with the symbology): 

![](result.png)

The specialist can now search where we are missing some towers! 



