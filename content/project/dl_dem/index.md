---
date: "2021-07-12T00:00:00Z"
external_link: null
image:
  caption: "Copernicus"
  focal_point: Smart
summary: "Downloading a Digital Elevation Model (DEM) from UE's Copernicus"
tags:
- Raster
- Input Data
- Visoka region
title: "Getting a DEM!"
---

<script>
function confortable() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>

# Copernicus provide a Digital Elevation Model for Europe

For the Visok border project we need a [Digital Elevation Model](https://www.univ-st-etienne.fr/wikimastersig/doku.php/english:glossary:dtm) (DEM) of this region. A DEM is usually a raster file (it can also be a vector with points or isolines) that stores elevation as a value of a pixel. 

You can get one of Europe [here](https://land.copernicus.eu/imagery-in-situ/eu-dem/eu-dem-v1.1). You will need to create an account. 

Downloading all european's countries will take a lot of space so Copernicus provides us with tiles. The one we need is the [E50N20](https://land.copernicus.eu/imagery-in-situ/eu-dem/eu-dem-v1.1?tab=download&selected:list=dem-v1-1-e50n20). Just this part will need us to download 5.8 GB. 

# Checking the metadata

Waiting for the download to complete we can consult the [metadata](https://land.copernicus.eu/imagery-in-situ/eu-dem/eu-dem-v1.1?tab=metadata). 

Here we can learn how and why this data was made available, learn how it was produced and we can get a nice summary of its format and resolution:  

> EU-DEM v1.1 is available in Geotiff 32 bits format. It is a contiguous dataset divided into 1000 x 1000 km tiles, at 25m resolution with vertical accuracy: +/- 7 meters RMSE.

We are downloading a [Geotiff](https://en.wikipedia.org/wiki/GeoTIFF) file with 32 bits (probably too much for our needs). The raster has a 25m resolution (each pixel is 25m x 25m). We could have tried to search for a more precise resolution but for this project it was enough. 

His vertical "accuracy" is around +/- 7 meters. RMSE stands for Root Mean Square Error or the standard deviation of the residuals. The **M** in DEM stands for Model (i.e. an abstraction of reality) so to test its accuracy the data producers have compared the DEM prediction to a number of points with exact elevation value and reported how the model matches the expected value.       

We will see [next](https://oleroy.gitpages.huma-num.fr/minerva/project/prepare_dem/) how to adapt this raw data.

