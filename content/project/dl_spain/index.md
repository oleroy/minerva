---
date: "2021-10-04T00:00:00Z"
external_link: null
image:
  caption: ""
  focal_point: Smart
summary: "Download all the data needed!"
tags:
- Input Data
- Cultural Trail
title: "Download Data from Spanish Government"
---


To prepare our cultural trail we will need some data. The Spanish government provides a nice [website](https://centrodedescargas.cnig.es/CentroDescargas/index.jsp) to get it. 

![](centro_de_descargas.png)

Here we will download: 

    1. Vector maps and Cartographic and Topographic bases

    2. Aerial photos 


The first one will provide you with a collection of vector data and the second will give you a picture from above of the area.

## Downloading Vector maps and Cartographic and Topographic bases

The [website](https://centrodedescargas.cnig.es/CentroDescargas/index.jsp) of the Spanish government allows you to pick either Spanish, English or French as language (top right corner). We went for English.

On it you can pick the `Vector maps and Cartographic and Topographic bases` and here select `BTN25` :

![](btn1_25000.png)

You will need to pick the `viewfinder`. You need to close one pop-up. We are interested in a cultural trail in Riópar so we will do a search with this toponym (*buscar por toponimo*)

![](buscar_por_toponimo.png)

In the results page, we have various format options. We will pick shapefile (Shape). The result starting with number correspond to the cartographic map's number. Here we know that we need both the 45066 add 45067 but a lot of time you will need to download a bit more and keep the only one you will need. 

![](reults_buscar.png)

We will get two zipped files. zip is a file format that act as a way to compress files and put them in a single file making them more easy to share.  

In preparation of our future works lets create a `directory` where we will store them. We create a directory called `cultural_trail` and in it we create a subdirectory called `data_raw` to store it. We will use the `cultural_trail` directory as the home or (`root`) of our future project. You will also have  to unzip all the files in the subdirectory. 

## Downloading Aerial photos

Let's go back in the main page of the *Centro de Descargas* but this time we will select `Aerial photos` 

![](aerial_photos_and_images.png)

Then select `Most recent ortophotos`:

![](PNOA.png)

We will go in the same interface to select the area (still Riópar) and here we will have to pick and download `PNOA_MA_OF_ETRS89_HU30_h50_0866.ecw` (this is a 3.6GB file so be sure to have a good internet connection and time).