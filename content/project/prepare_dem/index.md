---
date: "2021-07-13T00:00:00Z"
external_link: null
image:
  caption: "Copernicus"
  focal_point: Smart
summary: "Transform a Digital Elevation Model (DEM) to fit our needs"
tags:
- Raster
- Data Management
- Visoka region
title: "A smaller Raster"
---

<script>
function confortable() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>

# Various steps to transform and downsize our DEM 

We have [downloaded](https://oleroy.gitpages.huma-num.fr/minerva/project/dl_dem/) a huge raster (5.8GB). For our needs it is not useful to cover that extent and to be that precise. A smaller one will also help us focus on our area of interest and lower the time and cost in future processing. 


We will:
- Produce a vector to delimit our area 
- Clip/crop the raster with it
- Change the data type of each pixel to lower the size and round the value

# Reduce it's extent

If you remember our raster is 1000 x 1000 km. This is way too big for our project. To reduce it we will need to **crop/clip** it. We will use a shapefile and ask QGIS to remove everything outside of it. 

## Producing a quick shapefile

Here I wanted a rectangular shapefile, with the same CRS from my raster, so after creating an empty file for polygons[^1] I used the tool `Add Rectangle from Extent` from the `Shape Digitizing Toolbar`. This Toolbar needs to be added (right click on a panel then select it). It allows you to draw a perfect rectangle. I used the Villages vector's file as a starting point [TODO link].

[^1]: see [here](https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing/) if you don't remember how to do it 

![](mActionRectangleExtent.png)

I named it bbox_visok. "bbox" stands for *bounding box* and it is a slang/technical term of the computer science world that percolated into the GIS world. Normally it is the smallest box needed to contain all the objects you want. Here I increase it a bit and draw it by hand but QGIS also has an algorithm that will produce one (try searching "bounding" in the `Processing Toolbox`).

## Clipping/cropping a raster

Clipping and cropping are similar. QGIS, now, use more frequently **cliping**. To found the clipping tool search for it in the `Processing Toolbox`:

![](clip_toolbox.gif)


Selecting it provide us with this windows:

![](clip_raster.png)

The `Input layer` is our original DEM. For `Clipping extent` I selected `Calculate fron Layer` and picked the bbox_visok.shp file. We could have used `Draw on Canvas` that is quicker than producing a shapefile but our choice allows us to reproduce our clip if needed (for example in another raster or redo it on the same later if needed). Then in `Clipped (extent)` we decide to save our result in a file. 

We will talk a bit later of some `Advanced parameters` that we could have used to save us time. Stay patient and tuned!

Our new DEM is now ready and it is 188.4 MB. Way better but we can improve it a bit more.

# Adapting the raster data type to our needs and data

## Converting data type

If you look at our new DEM (or the old one) you can see that elevation is expressed with 7 digits in precision: 2 905,854492 as an example. The data type is `Float32` so for each pixel we are using 32 bits and we use some bits for keeping that precision (that is the `float` part). This is way to much, an integer (i.e. no float), is enough and 16 bits (value ranging from -32766 to +32766) is also good enough.  

A way to convert the data type is to use `gdal_translate`. As usual you can find it in the `Processing Toolbox` or you can go to the `Raster`'s menu -> `Conversion` -> `Translate` (AKA Convert format). 

![](raster_translate.png)

We set our `Input layer` as our new DEM. In the `Advanced Parameters` we pick `int16` in the `Output data type` and finally we save it! Here I went with  DEM_visok_int16.tiff. Now maybe you have already guessed but we used the same tools that the one we used to clip the raster (gdal_translate) we just used `Advanced Parameters`. The path that got us here was different and the interface was a bit different but in the background it was the same algorithm from GDAL. 

At the end of this process our raster is now 94.2 MB.  


## Reclassify by table/layer

Another option is to reclassify each pixel. To do that you can use in the `Processing Toolbox` the `Reclassify by table` or the `Reclassify by layer` tools. The idea behind them is that you provide a set of ranges of values (min and max) that will get a new value: 100.5 to 101.5 will get the value of 101. `Reclassify by table` requires you to type each range, which is a bit tedious (in our case) but can be handy if you want to reclassify for a low number of values (less than 5). So we created a  Geopackage (here just a spreadsheet) that contains 3 columns (+ the id columns) : one for minimum value an other for maximum value and the last one with the value wanted. 

Here you can get a glimpse of it:

![](break_table.png)

Then I used it with the `Reclassify by layer`:

![](reclassif_by_layer2.png)

The raster input is our clipped raster, the layer with a break is here called `break_table` (it is our geopackage), then we have to tell QGIS which fields store our minimum, maximum and output value and how to handle boundaries. In the `Advanced Parameters` we pick `int16` in the `Output data type` and finally we save it!

The process is a bit longer but at the end we get a 94.2MB raster file. 

This way looked a bit more complex but it has some pros:

- it is less of an hack (rounding value using a change of data type)

- We could have used other breaks: every 10 m, 50 m or 100 m 

