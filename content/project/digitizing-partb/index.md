---
date: "2021-05-05T00:00:00Z"
external_link: null
image:
  caption: 
  focal_point: Smart
summary: "Make a vector from an historical map part B: New Features"
tags:
- Vector
- Digitizing
- Input Data
- Coal Basin Saint-Etienne
title: Digitizing some data part-B!
---

<script>
function confortable() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>

The set up before editing was done [here](https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing/)

# Start Editing:

Now that we have an empty shapefile we need to edit it. For that you can (1) either right click on the layer (in the `Layers` panel) -> and pick `Toggle Editings` or (2) go go to the `Layers` menu and pick `Toggle Editings` (if you have several layers the one you want to edit should highlighted) or (3) you can press the icon:

![](toggle_editing_icons.png)

This icon is in the `Digitizing Toolbar`:

![](Digitizing_toolbar_off.png)

When you toggle the editing:

- you get access to more options in the `Digitizing Toolbar`:

![](digitizing_toolbar_on.png)

- a little pen show you which layer is in editing mode in the `Layers` panel

![](pendigit.png)

<button onclick="confortable()">Advanced Digitizing Toolbar</button>

<div id="myDIV">

If needed you can expand the `Digitizing Toolbar` with the `Advanced Digitizing Toolbar`. For that you need to right click on some `Toolbars` or `Panels` and select the `Advanced Digitizing Toolbar`. With its name it should be the first one in the `Toolbars`'s menu.

![](advanced_digitizing_toolbar.png)

(TODO describe them here or link to a project with more advanced need)

</div>

# Adding New Polygons:

## Create New polygons:

With our layer selected and in edit mode we can now add polygons. To do that we press the `Add Polygon Feature`:

![](add_new_polygon.png)

Now our mouse pointer is looking like a small target and we can left-click around the part of the object we want as a polygon. To tell QGIS that our polygon is complete (minimum 3 nodes) we use right-click.

![](add_new_polygon.gif)

We stayed at the same scale and we could have zoomed a bit more to do a slightly better curve on the north. No big deal, we will show you how to correct it later! Until you use a right-click you can still press `Esc` to cancel your polygon editing or pick cancel when prompted after right-clicking it. 

You can switch a bit the scale while digitizing but using scales of other order of magnitude could lead to some false data heterogeneity and should be avoided.

![](second_polygon.gif)

We can see that our snapping rules work: our target turns purple and sticks to the other polygon.

It is good practice to save it frequently. `Ctrl+S` works or you can press:

![](save_it.png)

The little pen in the layer will be brown if you have unsaved edit.

## Edit/delete them:

Changing the symbology a bit is helpful. For that you can right-click on the layer in the `Layers` panel -> go to the `Symbology` Tab and lower the `Opacity` to 40% (or something similar). 

We were a bit far off so let's correct it. Lets select the `Vertex Tools`. 

![](vertex_tools_icon.png)

We get a new panel `Vertex Editor`. Right-Clicking on a feature (aka a polygon here) will allow you to see a table of coordinates of each vertex for one polygon. It has a ton of options but just checking if we have a duplicate vertex (let's say too many clicks) can be a time saver. 

![](vertex_editor_panel.png)

Here we see how after zooming a bit we can correct our second polygon. We also see that our snapping can be a bit of hindrance and should be lowered a bit (at least now). 

![](correcting_nodes.gif)

If you want to delete a polygon (or any features) you need to select it. For that you can use the `Select Feature` icon (TODO link to various way of selecting):

![selection_tools.png]

Then when the feature is highlighted you can delet it with one of the `Delete` key or with the `Delete Feature` icon: 

![](delete_feature_icon.png)

You can use all the keyboard shortcuts like `Ctrl+Z` for *undo*, `Ctrl+shift+Z` for *redo* and `Ctrl+S` to save it. They can also be found in the `Edit` Menu.

Newcomers tips: It is easy at first to confuse the `Saving Projects` icon:

![](save_project_icons.png)

With the `Saver Layer Edit` one:

![](save_edit_icon.png)

The first one saves the project and the second only the edit you have done to the layer you are editing. 

Adding information into each feature is our [next step](https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing-partc/)!