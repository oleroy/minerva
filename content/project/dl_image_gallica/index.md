---
date: "2021-04-15T00:00:00Z"
external_link: null
image:
  caption: 
  focal_point: Smart
summary: Downloading data from Gallica's web site
tags:
- Raster
- API
- Input Data
- Coal Basin Saint-Etienne
title: Getting some data!
---


Gallica is the digital library of the Bibliothèque nationale de France. A lot of [historical maps](https://gallica.bnf.fr/html/und/cartes/cartes?mode=desktop) have been digitized and are available.

We are interested in a map of the coal basin in Saint-Etienne, France. We have found [one](https://gallica.bnf.fr/ark:/12148/btv1b53085116w#) but we can't download it with a good resolution. 

If we want to do that we need to use gallica's API. API stand for *application programming interface*. It is an interface that allows us to have more options than the default's one. 

The documentation of this API can be found [here](https://api.bnf.fr/fr/api-iiif-de-recuperation-des-images-de-gallica#scroll-nav__4). It's a bit cryptic at first but they provide examples and making some mistakes will not cost you anything ! 


The request look like that : 

```
{scheme}://{server}{/prefix}/{identifier}/{region}/{size}/{rotation}/{quality}.{format}
```

The `{scheme}://{server}{/prefix}/` will most likely always be the same. We will use `hhtps` protocol with `gallica.bnf.fr` server with `iiif` prefix. In short the first part is : 

`https://gallica.bnf.fr/iiif/`

The `{region}/{size}` represent our map; it can be found in the url provided by gallica (`ark:/12148/btv1b53085116w/`). In fact Gallica probably uses the same API to create the pages in our browser. 

Finally in the tail we will say that we want the first page (`f1`), all the page (`full`) a specific resolution (`8000,4000`), no rotation (`0`) and the image in `.png`.

The process involves some failure if you don't know the API so don't be afraid of 404 !  


This is the final request ! 

```
https://gallica.bnf.fr/iiif/ark:/12148/btv1b53085116w/f1/full/8000,4000/0/native.jpg
```

Now we need to add it to QGIS ([here](https://oleroy.gitpages.huma-num.fr/minerva/project/georeferencing/))!


