---
date: "2021-09-20T00:00:00Z"
external_link: null
image:
  caption: ""
  focal_point: Smart
summary: "Learn how to be productive with your field survey"
tags:
- Input Data
- Tuscan Coast Towers
title: "Preparing a Field Survey"
---

A lot of times we need to collect data from the field. It can be done through a lot of ways: GPS, photos, use of UAV (Unmanned Aerial Vehicle like drones), audio recording, measuring tools and more!

It will be impossible to cover everything but there are few key pieces of advice we can share and illustrate with this case. 

We will not discuss what kind of data should be collected.  

## Like everything, plan it well! 

Collecting data on the field is usually a time consuming activity and planning it well can save you a lot of trouble! 

First, you need to be sure that you have everything for your collecting session (make a list 📋!). Now we can use pad or field computer to directly encode information into a spreadsheet but it is still useful to have your spreadsheet in paper (or just a notepad with pen) in case your field computer run out of battery or break (a lot now also can be done with a smartphone). 

The other time consuming activity is to bridge your new data to your GIS [TODO link to pre-processing when it's done]. Having thought about it before will save you a lot of time (how should I store this data, how do I represent it?). 

Finally, you should think how you will share it with others: how will they access information, what format (paper or digital, pdf, excel or shapefile etc) will they be able to use and how you can collaborate with it. 

Last advice: be flexible! Even if everything is well planned a lot can happen in these steps and that is totally normal. 

## Preparing for the Tuscan Coast Towers field:

The goals of each field survey will be to:

a) Provide a quick description of each tower. 

b) Stake out the area of interest.

c) Take geolocalized pictures of the tower and from the tower.

The second goal (staking out the area of interest) will taken care of by your GPS. It will provide you with the coordinates usually in the GPX format. You can be extra cautious and write them in a notepad. But for the first and third one we will need a field survey. 

### Every GIS survey usually include "Who", "When" and "Where"

Traceability and the ability to share are important in GIS, so every field survey should include the names of people who are doing it (and sometimes a way to contact them). Having the date (recorded in a consistent way) when the survey was done is also important to evaluate the "freshness" of the data or can help later to explain future changes. After the *"who"*, *"when"* then the *"where"* is one of the core of GIS. *"where"* is a complicated topic, to allow various scales we usually add to the coordinates the administrative layers (ex: province, municipality). 

Another very common information recorded in every field survey is the accessibility of the site. A lot of time this data lives in "reports" / "observation" field (here its name is `Reports`). 

Sometimes this *Who*, *When* and *Where* are categorized as **metadata** or are at least part of it. 

In case of the Tuscan Coast Tower project you can see that we have used "Date of Survey" for *When*, for the *Who* it has been devised in "Topographer", "Photographer", "Archeologist" and the *where* is also devised in "Province", "City", "Locality".

![](header_description.png)

Bigger the project and as increase the number of people doing field work the more those information are important.

For categorical variables that can only take a predetermined range of value (a good example can be who is doing the survey) it is useful to set up with Excel or LibreOffice a little scrolling menu. In both of these software it is in : the `Data` menu ->  `Validity` -> `List` where you can add all the values that this cell will accept. 

This will help avoid some common misspelling and can save you hours of "cleaning" data! 

### After the metadata the data

#### Describing the tower:  

![](data_part.png)

This will depend of each project but here you will find: 

* The Tower name (`Name`) 
* A quick description of it:
    - its `Category`,
    - `Kind of structure` 
    - `Description`
    - `Time period`
    - `State of preservation`
* The name of the shapefiles that will be used: 
    - `Shape point location`
    - `Shape site area`
    - `Shape photo`

#### Referencing the geolocalized pictures of the tower and from the tower


The photos will be taken in a round clockwise first from the tower then from outside to the tower. Here we decided to provide a code `photo_cod` that matches every situation and is also reported at the end of the picture. The name of every picture follows this pattern: `nameoftower_photo_cod`. 

![](photos.png)

We didn't need to take pictures of `Particular element` and `From above with drone` so we added `NA`. NA stands for Non-Available. It is better to report with NA than letting it blank : a blank field can be interpreted as someone forgetting to do it where NA means explicitly that it was not available or not needed. 

We didn't specify  the metada here because this form is a sheet in the same sheet that the description's one.

Finally, it is important to provide a good name to your spreadsheet. Here we go with `Mathilde_tower`, `_` is better than ` ` (whitespace) because some software doesn't do well with them. For the same reason avoid special characters like ` ' or accented letters.




