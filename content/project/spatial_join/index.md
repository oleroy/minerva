---
date: "2021-09-03T00:00:00Z"
external_link: null
image:
  caption: ""
  focal_point: Smart
summary: "Joining attribute from polygons to points in it"
tags:
- Data Analysis
- Seismic Hazard
title: "An example of spatial join"
---

We start from our [extract](https://oleroy.gitpages.huma-num.fr/minerva/project/selection_filter/) of buildings in CH in Slovenia.

We want to add the `POSPESEK` field (or attribute) to each of our buildings in CH. This is what we call a **Spatial Join**. Before learning how to do it with QGIS let's learn a bit how it works.

## Spatial Join in Theory:

Spatial joins depend on two ideas. The first is what we call **topological relations** producing **spatial predicates**. Those are the rules that help us answer what is the relation between two spatial objects. An example of questions can be: "Is this point included in this polygon?". We are asking for the topological relation of a point with a polygon and we are asking a specific type of relation with *include* (official terminology is **intersect**). The answer will be either TRUE or FALSE. We could have chosen other predicates, for example, *not include* (ie **disjoint**) or *less than 10 m away from it*. The results of each relation will depend on the second idea: the **spatial object** (points, lines, polygons). 

If we ask if a polygon (A) contain another one (B), the answer can be: 

- Yes, totally!

- Partially (kind of yes here and not here) 

- No.

For a point "partially" makes no sense (points don't have area by definition).

Here you can visualize the possibility of topological with their relation: 

![](spatial_predicates.png)

With that we can now have an index of which points that are following  topological relations. In our example we want to attribute the correct `POSPESEK` value to each point inside a particular area. We, with the help of QGIS, will realize an **intersect** and add for every point that matches our criteria their `POSPESEK` value. 

We have different values of `POPSESEK` for each polygon. QGIS will see which points intersect each polygon and then attribute the correct value of `POSPESEK`. For every point if it intersects a polygon you will now have a long list of **Yes** or **No** (the list is as long as you have polygons). Then it will attribute the value for each **Yes**. Repeat that for every point. This is very tedious but computers are good at this kind of task. 

This is a spatial join!

![](sp_join.png)

## Spatial Join in Practice: 

### Creating a Spatial Index: 

It is quick for us to see if a point is or is not inside a polygon. But how does a computer handle it? To make it simple it does a bunch of topological operations. Those operations can be a bit long to do so we are using a **spatial index**. We (well the computer will do that for us) divide the space in squares and record if our lines, points or polygons are in it or not and we do that at various nested scales. With that it will exclude a lot of space where we know our features are not here and it will lower our topological operations. 

It is easy to do it with QGIS. It is a vector operation so we go to the `Vector` menu -> select `Data Management Tools` and pick `Create Spatial Index`. 

![](spatial_index.png)

Now with the power a lot of computers have this step is not always needed but when you will have a huge join to do remember it!

### Doing the Join:

Spatial is also a Vector operation. We can go to the `Vector` menu -> select `Data Management Tools` and pick `Join attributes by location`.

The `Base Layer` is the layer that will inherit the new column. Here it will be `buildings_CH`. Note that it can also work on `Selected features only` which is a nice short cut sometimes. The `Join Layer` is the one that provides the new value (here `Seismic Hazard - Design ground acceleration`). We pick `intersects` as a predicate. It is even possible to add more predicates (but be careful with this powerful option)! 

Finally, we just have only one field so no need to change that. Let's press `Run`!

![](join_attributes_by_location.png)

We have now a `Joined Layer` in our `Layers` panel. As you can see with its microchip on the right this layer is temporary and will need to be saved later. 

![](joined_layer.png)

That's it! You can check at the end of the attribute table in your new layer that you have a `POSPESEK` field. 

## Caution!

It was easy here but it can *fail*. What happens if a point intersects a border between two polygons? Well, it will depend on how the algorithm behind it is designed. You will either get an error message or it will not take into account the second polygon (or more complex options). Also it becomes a bit more complex with lines and polygons: what happens if a lines intersects two (or more polygons)? Same question with intersecting polygons. 

Spatial Joins are powerful tools but the user needs to think a bit before using them and visualize what are the potential outcomes. Doing some quick drawing can help a lot.  

