---
date: "2021-05-05T00:00:00Z"
external_link: null
image:
  caption: 
  focal_point: Smart
summary: Wrap a raster in another CRS
tags:
- Raster
- Data Management
- Coal Basin Saint-Etienne
title: Reprojecting a raster!
---

# Preamble

1. Download the historical map: [here](https://oleroy.gitpages.huma-num.fr/minerva/project/dl_image_gallica/) 

2. Georeference it : [here](https://oleroy.gitpages.huma-num.fr/minerva/project/georeferencing/)

# Reprojecting it or Warp it!

This historical map should be assigned with the coordinate Reference System (CRS) of `WGS 84 / Pseudo-Mercator` also known as EPSG: 3857. 

You can check that with a right-click on the layer (in the `Layers Panel`) -> select `Properties` -> go in `Source`'s Tab and see it in `Assigned Coordinate Reference System (CRS)`

![](assigned_crs.png)

In France the recommended CRS is `RGF93 / Lambert-93 -- France` or EPSG: 2154. It is a projected system. If we want to change it; we will need to warp or reproject (warp and reproject are synonyms) the raster in the desired system. 

It can be done with the `Raster Menu` or with the `Processing Toolbox`.

## With the Raster Menu:

In the `Raster Menu` go to `Projections` and pick `Warp (Reproject)`.

![](raster_wrap.gif)

## With the Processing Toolbox:

The `Processing Toolbox` is not always visible, to add it you will maybe need to right-click in a panel and select it:

![](add_precessing_tb.gif)

You can search `Warp` and find it:

![](processing_tb_warp.png)

## Warp Options:

These two options will lead you to the same window.

![](warp_raster.png)

Let's spend a bit on the tab `Parameters`: 

- Our Input will be the raster we want to warp

- The Source CRS is said to be optional because the software can detect it but if, for some reasons, the file was not linked with a CRS it should be added

- Target CRS is also said to be optional but in our case we have to set it up. Warp can also be used to resample (see next option) so in this case you will maybe want to keep the same CRS.

- Nodata should not be change [TODO raster and bbox to explain why]

- We don't want to change the resolution of our raster so we didn't change the Output file resolution in target georeferenced units [TODO also related to raster]

- Finally we saved our output in a file

# GDAL: Spatial Data Swiss Army Knife 

You may have noticed that every time we change something the `GDAL/OGR console call` is modified. This is a bit more advanced topic but it is good to learn that behind the scenes QGIS is using the GDAL library. For this operation it uses gdalwarp.

GDAL stands for Geospatial Data Abstraction Library. It is an open source project and free software (thanks to the work of a lot of people). You can find more about it on it's [website](https://gdal.org/index.html) and see about all the options of [gdalwarp](https://gdal.org/programs/gdalwarp.html) in its documentation. 

