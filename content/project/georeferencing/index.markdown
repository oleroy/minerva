---
date: "2021-04-16"
external_link: null
image:
  caption: 
  focal_point: 
summary: Georeferecing an image
tags:
- Raster
- Input Data
- Coal Basin Saint-Etienne
title: Getting an historical map in QGIS
---

<script>
function confortable() {
    var x = document.getElementById("myDIV");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
</script>

# Loading the Historical Map

## Lost in Null Island 

We have an image from Gallica[^1] and now we need to add it to our GIS project.

[^1]: We have done that [here](https://oleroy.gitpages.huma-num.fr/minerva/project/dl_image_gallica/). 

If you add it directly (drag and drop the image in the `Layers Panel`) it would appear in the Atlantic Ocean, west of Africa! Not that close to Saint-Etienne, France.

![Background Map: Open Street Map © OpenStreetMap contributors](Coal_basin_null_islandv2.png "Background Map: Open Street Map © OpenStreetMap contributors")

Don't panic, you are now in "Null Island"[^2]. It's a fictional Island with Coordinates **0°N 0°E** and when we mess up something GIS practitioners end here. This happens a lot so why not give it a name! 

We ended up here because a `.jp2` file also uses a coordinate system for the pixels starting at **0, 0** (top, left). To avoid that we will need to **georeference** it: translate and rotate it to the correct place. 

[^2]: See [Wikipedia](https://en.wikipedia.org/wiki/Null_Island) and this [lost blog](https://blogs.loc.gov/maps/2016/04/the-geographical-oddity-of-null-island/) post about-it 

## Using the Georeferencer

 We are giving it *georeferences*. This image will be what we called a **raster** [TODO raster link]. 
For that we will need to use the **georeferencer**. It can be found under the raster menu.

![](georeferencer.png)

It will open a new window that look like that: 

![](georefglobal.png)

It's fine if you don't understand all the options and icons here. Some are also redundant : you can find them in the menus or in the toolsbars. QGIS provides a full description of them [here](https://docs.qgis.org/2.18/en/docs/user_manual/plugins/plugins_georeferencer.html).

First we will need to add the image we want to georeference. This can be done by going to the menu: 

`file` and pick `open raster`. You will be asked to select the image. 

You can also just click on this icon ![](load_raster.png)

This is what it should like:

![](cartescharge.png)

# Adding GCP!

GCP stands for Ground Control Point. The basic idea is that we will select points in our image where we know the exact (or mostly) location. It can be very easy, some maps have it implemented as you can find the exact coordinate in each corner but in our case it will be a bit harder: this is an old map with less than optimal localization. 

In QGIS we have added previously a WMTS of IGN (TODO link to how to do) and we will use it as a canvas to locate our points. 

To add a GCP:

1. Click on: 

![](addpoint_georef.PNG)

2. Click on an easy localization. This is a difficult task and you will need to navigate between the georeferencer windows and the project windows to find localization that match. Making mistakes is perfectly fine; you can later either delete or move your GCP point. On a historical map you have to find some place that didn't move between time: river can be good, village center with a church are also usually a good bet and so on.

After founding a good places and clicking on it this window will pop-up: 

![](enter_map_coordinate.png)

3.  We can add the X / Y if we know them but here we will go and click on `From Map Canvas` 

4. Then you pick on IGN'S WMTS the point of the localization 

Now you should be able to see a new entry in the GCP table (in the georeferencer menu).

![](newpoint_vis_georef.png)

In the table you can see the coordinate of the Source (`Source X`, `Source Y`) and then the destination (`Dest. X`). We will explore a bit later what `dX (pixels)` , `dY (pixels)` and `Residual (pixels)` means.

5. Then you will have to repeat this operation. This can be tedious and a bit difficult if you are unsure you can still remove a point later. 
In this example we have added 52 points. The number of GCP you should add depends on the deformation of the map and the precision you want to get. 

6. You should:
    - at least add 4 points and it is common to add 30
    
    - the points should cover all the raster 
    

It is good practices to save your GCP: 

With this button : ![](save_gcp.png)

Or : `File` -> `File Save GCP ...`


**Tips** : you can use other resources! Here we also checked old aerial photographs to look for potential changes. 

# Applying a Transformation

When you think you are done with adding GCP. We will transform the image to add spatial coordinates.

You can use this button : 

![](transform_setting.png)

Or in the menus : `settings` -> `transformation settings`

This new window will be prompted: 

![](transformation_settingsv0.png) 

This news window has three parts. The first one is about `Transformation Parameters`, the second `Output Settings` and the last about `Reports`. 

First you will have to choose an algorithm that will transform the image (`transformation type`).  Here we picked `polynomial 2`  and `Linear` as Resampling methods (see "More information" to learn more about it). We also tried `Polyomial 1` and other resampling methods but the results weren't great. The target SRS should point to the EPSG of your QGIS project.

<button onclick="confortable()">More information</button>

<div id="myDIV">

## QGIS offer 5 algorithms of transformation:

The basic idea is that you will use GCPs and an algorithm to produce a new image that can be rescaled, rotated and even distorted.  

- The **linear** algorithm only allows for a translation of the image and no rotation. It is useful for very good raster maps with a known CRS and an uniform scaling.

- The [**Helmert transformation**](https://en.wikipedia.org/wiki/Helmert_transformation) allows for a rotation and an uniform rescaling. It is used mostly for good quality maps or aerial images not correctly aligned.

- The **polynomial 1** or first order polynomial algorithm is the first one that allows for an [affine transformation](https://en.wikipedia.org/wiki/World_file). It ensures that line and parallelism are preserved but not necessarily distances and angles. It enables scaling,  rotation and atranslation. It requires at least 3 GCPs and adding more than 6 will not increase  your accuracy (granted that these points are well positioned). 

- The **polynomial 2-3** algorithms are needed to handle localized curvature or systematic warping of an image (think of a bad book scan with book binding). Straight line and parallelism will not be preserved. More GCPs are needed. Scale will also not be preserved or treated uniformly across the image. Going for the third order allows for more specific distortion witch can be good if needed but can also lead to unwanted distortion. 


- The **Projective** algorithm allows for the lines to stay straight but for that can change parallelism and scale can differ in the same image. It is used for oblique aerial images. 

- The **Thin Plate Spline (TPS)** algorithm is using multiple local polynomials to match the GCP. Areas away from GCP will be moved so the more good GCP the better. It is useful for approximately georeferencing and implicitly reprojecting maps with unknown projection. 

A lot of time you have no or poor information on the quality of the image you are trying to georeference so it's good practice to try several transformations and see what seems to give you the best result. 


## Resampling methods

The aim of georeferencing is producing a new image, aka in the GIS world as a *Raster*. Resampling methods are used to determine the value of the pixels that will be generated. QGIS offer you 5 options :

- **Nearest neighbor**: The value of each pixel in the output image will be determined by the closest pixel in the input. 

- **Linear**: This one uses the four nearest pixels and weights their values with their respective distance to the output pixel. This method smooths the output. 

- **Cubic**:  This one is similar to the linear one but takes 16 pixels value from the imput image. 

- **Cubic spline**: This one is based on a spline function but is not well documented (how many knots are used?).

- **Lanczos**: This one was more poorly documented so a bit of research was needed. Here the reader will find a good piece on [it](https://gis.stackexchange.com/questions/10931/what-is-lanczos-resampling-useful-for-in-a-spatial-context). It uses a *convolution matrix* called sometimes a *kernel* or *windows*. This is a grid with weight associated at each cell and the weight associated came from a `sinus x / x` style function. It will enhance the local contrast.  

Last advice: Only the **nearest neighbor** will keep the same range of value, it will not create a new one. If you want to avoid creating a new value, for example a categorical value, you should pick it. 

</div>

Before starting the georeferencing it can be good to sort by decreasing your GCP with the column `residual (pixels)` in the GCP table. 

![](apres_transfo_polynomialv2linear.png)

This will show which GCP will be moved if you do the georeferencing. You can then try and see if some points should be moved. Here you can see one of our mistakes that needs a correction. 


![](une_erreure.png)

To correct it you just select the `move GCP point` icon and move the point in the map canvas:

![](modifier_point.png)

Sometimes nothing can be done and then it is also an option to delete a GCP. This entire process is a lot of trial and error.

When the result looks good you should reopen the `Transformation Setting` window and set up the path and name of the new output raster then you are done and if not selected you should ask to load it in QGIS!

You can then start the georefercing : `file` -> `Start Georeferencing` or press this icon:

![](start_geoprocess.png)

Now in the layer panel you should have your new raster. You can go into their properties and set up a bit of transparency to see how it maps your map canvas. If you have done multiple transformations you can then see how they compare to each other. 

[Next](https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing/) we will show you how to digitize it! 

(TODO linking or introducing world file and header in geotiff/tags)





