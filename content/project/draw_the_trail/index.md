---
date: "2021-10-12T00:00:00Z"
external_link: null
image:
  caption: "[Musel de las Reales Fábricas de San Juan de Alcaraz](http://www.museofabricasderiopar.com/)"
  focal_point: Smart
summary: "A shapefile to draw the trail"
tags:
- Data Management
- Cultural Trail
title: "Creating the Trail"
---


We will use or project to represent a trail that go around various factory of Riópar.

In the Qgis toolbar we will select the `Layer` and use  `Create Layer` and select `New layer from shapefile...`:

![](create_layer.gif)

A dialog box will appear where we will see several fields to fill in:

- The first of them will be the name. Click on the button with three ellipses points (`...`), this is used to save the path where our files referring to the layer will be located and also to give it its name. In our case, `trail_riopar`.

- The file encoding will be "UTF-8". Computers understand bits (ie 0/1) and sometimes we prefer letters so we need a system that allow conversion between both sides. For example in UTF-8 `01100001` means `a`. We can and we have come up with a bunch of ways of doing that but UTF-8 is a shared standard so you should pick it.  If you pick "system" it will be the one implemented in your operating system (OS) and if someone or if you change it you do not have any guarantee that it will be the same.

- Geometry type will be a line (`linstring`) since this is probably the best option to display a trail. 

- We have selected the appropriate spatial reference system. In our case, it will be EPSG:25830/UTM zone 30N.


-  As for the moment we are only going to draw lines so we do not need any additional field other than the default one.

- Then accept !

![](new_shape_layer.png)

Digitization is a process of incorporating new information into the system. Layers can be loaded with existing information as we have seen and new information can be created. The incorporation of new information by digitizing is currently done by digitizing on screen from pre-existing georeferenced cartographic bases. The digitizing is done in vector format (points, lines and polygons), being able to be later rasterized if it is of interest to transfer it to another format. In our case for a route we will work in vector format.
