---
date: "2021-10-05T00:00:00Z"
external_link: null
image:
  caption: ""
  focal_point: Smart
summary: "Setting up a new project for our cultural trail"
tags:
- Data Management
- Cultural Trail
title: "Managing Data by doing a QGIS project"
---

Let's say you have downloaded all the data from [here](https://oleroy.gitpages.huma-num.fr/minerva/project/dl_spain/). Before jumping into drawing our trail we need to organize our project a bit. For that we will do a QGIS [project](https://oleroy.gitpages.huma-num.fr/minerva/project/dl_spain/) linked with a hierarchy of directories. 

There are other way to organize your work, for example the raw data can be stored in a shared hard drive in your institution network, but it is good to have and keep one structure.

## A Project directory:

### A clean directory to start: 

If you have followed the step in the previous part you should have a directory (here: `cultural_trail`) for your project with a subdirectory with `data_raw` that contains the shapefiles and aerial photos that we have downloaded. 

Something like that:

![](tree_cultural_trail.png)

You may have a slightly different path. Here, on linux, mine is `/home/lo82302h/Documents/cultural_trail`; `/` is the beginning of my hard drive (its *root*). On Windows it should look like that : `C:\Users\lo82302h\Documents\cultural_trail\`. Windows hard drives are named (here it is `C`). You can also notice the use of slash (`/`) or backslash (`\`). 

The differences between the way these two paths are written are not very important. What is important here is that this two path (a path is an abstraction to help you and the computer to locate the file or directory in its memory) are **absolute** it means that if you start the path (to keep drawing in the analogy) you will go in the cultural_trail directory. A path can be **absolute** and **relative**. We will give you an example soon of them and it will be more clear why you need to keep them in mind!

From a new `Untitled Project` we can save it in Qgis going into the `Project` menu and use `Save As...`. We will then save it in the `cultural_trail` directory. To be original we will name this project `cultural_trail.qgz` (Qgis take care of adding `.qgz`). 

![](saving_project.gif)

Now you should have something like that in your cultural trail directory:

![](adding_qgz.png)

A Qgis project is a way to record parts of your QGIS session. 

### Adding informations to your project:

Now that you have a project defined Qqgis will provide you with a special link in the `Browser` panel where you can access related data:

![](project_home.gif)

#### Setting CRS:

Before adding layers let's start by defining our `Coordinate Reference System (CRS)`. By default Qgis use the [EPSG 4326](https://epsg.io/4326) also known as WGS84 or World Geodetic System 1984. In this project, in Spain we need to use the ETRS89 / UTM zone 30 also known with its EPSG code [25830](https://epsg.io/25830). How do we know that? Well we are just following [guidelines](http://www.ign.es/web/ign/portal/preguntas-frecuentes) from Spain's *Instituto Geográfico Nacional*. 

UTM stands for Universal Transverse Mercator and is a cartographic projection (a way to go from our 3D earth to a 2D plan) and ETRS89 stands for European Terrestrial Reference System 1989 is a geographic coordinate system (3D) so we assume that 25830 is an UTM based on ETRS89 [TODO asking Spain team to be sure].

To do that we have two options:

* You can access the CRS at the bottom right corner:

![](bottom_right.png)

* Or inside the `Project Properties` window (Menu `Project` -> `Properties..`) inside the CRS tab.

Both way will lead to the CRS tab:

![](PP_CRS.png)

To find the CRS you can use the `Filter` with the EPSG code (25830). Then you need to  click on it and `Apply` or `OK`.

Then if you save your project (ctrl + s) or click in:

![](save_it.png)

This will save that information in `cultural_trail.qgz` so you don't need to redo it each time!


#### Adding layers:

Each of the folders`45066` and `45067` contains around 44 shapefiles. If we want to inspect them it is a good idea to create groups. Here you also have two options to do it.

The first one you start by creating a group and drop layers in it. The second one you drop the layers in the `Layers` panel then group them. 

To add a group layer you just press the `Add Group` button and type his name:

![](add_group.gif)

For the second option we are also showing you how to grab a bunch of layer from the `Browser`, select one then go at the end of all the layers you want to add, press shift and while press it click on the last layer, Then you just drag and drop all of them in the `Layer` panel. In the `Layer` you select all the layers you want to regroup and then right click to pick `Group Selected`.

![](re_group.gif)

At the end the Layers panel in my project look like that:

![](layerfinparam.png)


Finally, you can add OSM ([OpenStreetMap](https://wiki.osmfoundation.org/wiki/Main_Page)) layer simply by dragging it from the Tiles services to your `Layers` panel and have that really nice tile of the world! 