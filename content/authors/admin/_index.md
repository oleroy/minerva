---
authors:
- admin
avatar_image: Logo_evs.png
bio: NA.
btn:
- label: Project website
  url: https://minerva-erasmus.com/
display_education: false
email: ""
interests:
- Vector
- Raster
- QGIS
name: M.
organizations:
- name: EVS
  url: ""
role: EVS
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: twitter
  icon_pack: fab
  link: https://twitter.com/Minervaproject3
- icon: google-scholar
  icon_pack: ai
  link: 
- icon: github
  icon_pack: fab
  link: 
superuser: true
user_groups:
- Researchers
- Visitors
---


Examples for toolbox 


