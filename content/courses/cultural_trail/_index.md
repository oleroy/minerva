---
date: "2021-09-23"
draft: false
lastmod: "`r Sys.time()`"
linktitle: Cultural Trail, Spain
summary: Design and Elaboration of Geo-Referenced Cultural Itinerary in Riópar, Spain
title: Cultural Trail, Spain
toc: true
type: docs
tags:
- Cultural Trail
menu: cultural_trail
---

## Objectives

The main objective of the activity is the creation of a route in areas of cultural interest, both human and natural, using GIS as the main tool. This general objective can be broken down into several smaller objectives.

- To understand the importance of GIS in relation to heritage.
- To understand and apply the concept of Geolocation and Geocoding. - To plan and manage geospatial information in a Geographic Information System.
- To value cultural and natural heritage through the design of a route.


## Skills and level of Competences

At the same time, these objectives are specified in a series of skills and abilities that the student will acquire

### Level of Competence: 

Basic to advanced

### Skills to be learned: 

- Basic use of GIS software
- Import, export and management of georeferenced data.
- Digitalization
- Installation and use of [plugins]
- Management and knowledge of spatial reference systems
- Production of cartographic outputs
- Use of the field calculator

## Workflow

{{<mermaid>}}
graph TD
  A[Download national Cartographic databases]--> B[Rank Symbology]
  B--> C[New Shape file for the trail]
  C--> D[Filtering Data of Interest]
  D--> E[Adding attributes with Field Calculator]
  E--> F[Create symbology]
  F--> G[Export un KML format] 
  click A "https://oleroy.gitpages.huma-num.fr/minerva/project/dl_spain/"
{{</mermaid>}}


