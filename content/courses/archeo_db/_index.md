---
date: "2021-06-10"
draft: false
lastmod: "`r Sys.time()`"
linktitle: Tuscan coast towers and forts, Italy
summary: Creation of a database from field surveys and bibliographic studies  
title: Tuscan coast towers and forts
toc: true
type: docs
tags:
- Tuscan Coast Towers
menu: archeo_db
---

## Context

The coastal towers and forts of the littoral are part of the historical and architectural heritage of Tuscany. In the course of time, some of them have been completely destroyed and the traces can be found only through documents, archives and historical maps. For others, only the ruins remain or, sometimes, as in the case of the Matilde Tower of Viareggio, restoration and renovation works have saved them.  


## Geographical aspect 

Studies have reconstructed the coastal military organization of Tuscany (Maretti, 1991; Guarducci et al., 2014) through the use of cartographies, iconographies, photographs, written accounts, etc.

In total, more than 160 sites of fortified architecture along the coasts have been archived. It's a very extensive network that extended from the border with Lazio, with the armed post of Graticciaia, to the Mouth of the Magra River. They were also present on the islands of the Tuscan archipelago (Gorgona, Capraia, Elba, Pianosa and Montecristo).


![Torri e fortezze della Toscana tirrenica, Guarducci e al., 2014](guarducci2014.png)


## Intervisibility analysis  

### Case Study  

The following case study presents a methodology for creating a digital database using previous studies and archival documents, completed by field surveys and 3D reconstruction.

We have chosen 4 towers for the creation of the database:  

  * Matilde tower (Viareggio)
  * Fortino nuovo (Migliarino)
  * Batteria Bocca di Serchio (Migliarino)
  * Migliarino's fort (Migliarino) 

Field surveys's Instruments :

  - GPS
  - Compass
  - Photo camera with integrated GPS
  - UAV 
    
Compilation of a field survey form with the following information:

  - Exact location of the property
  - Area of sediment
  - Photograph from and to the property from the 8 cardinal points
  - Photography of particular 
  - State of preservation
  - Brief description

## Skills and level of Competences

### Level of Competence  

Basic to advanced

## Skills to be learned:

  - Methodology to realize field surveys for cultural heritage (using GPS, relevant information).
  - Transform GPS data into a shapefile. 
  - Create Geodatabase with field surveys data.

## Workflow


{{<mermaid>}}
graph TD
  A[Field survey]--> B[Extraction of GPS data]
  B--> C[Compilation of the form]
  C--> D[Data analysis]
  D--> E[Data creation]
click A "https://oleroy.gitpages.huma-num.fr/minerva/project/field_survey/"
{{</mermaid>}}



## Programs used:

  - Geo Setter
  - Google Earth
  - Excel
  - QGIS 


## Data

- Archaeological site map
- [Geoscopio Toscana?](http://www.datiopen.it/it/catalogo-opendata/archeologia)


