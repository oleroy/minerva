---
date: "2021-08-24T00:00:00Z"
draft: false
lastmod: "`r Sys.time()`"
linktitle: Mapping Natural and Human Made Borders, Serbia
summary: The small, mountain historical and geographical region of Visoka consists of a few dozens of small pastoral villages along the Visočica River, which flows on the Stara Planina Mountain, and northern to the Nišava River. The local picks and ranges separate the highland region of Visoka from the neighboring small-size geographical units, making it distinct and seclusive. The area is now divided between the Republic of Serbia (biggest part) and the Republic of Bulgaria based on the borderlines set in 1878 and 1920 with no regard for the local ethnographic or ethnic situation.  
title: Mapping Natural and Human Made Borders
toc: true
type: docs
tags:
- Visoka region
menu: Visok
---

## Introduction

The small, mountain historical and geographical region of Visoka consists of a few dozens of small pastoral villages along the Visočica River, which flows on the Stara Planina Mountain, and northern to the Nišava River. The local picks and ranges separate the highland region of Visoka from the neighboring small-size geographical units, making it distinct and seclusive. The area is now divided between the Republic of Serbia (biggest part) and the Republic of Bulgaria based on the borderlines set in 1878 and 1920 with no regard for the local ethnographic or ethnic situation at the time of partition.

One may observe and research the region of Visoka both as a historical and geographical/environmental phenomenon. 

## Historical and human aspects:

The history of the Visoka region is not well-known, predominantly due to the scarcity of the written historical documents. However, the earliest Ottoman Census Books (Defter) provide an interesting insight into the middle and late 15th-century local history. At that time, the region was the undisputed part of the Ottoman Empire, although the upper part of the Nišava region had the border character in the previous period. Namely, it was where the Serbian realm and three Serbian Principalities (Principality of Duke Lazar – until 1289, Dragaš Family estates – until 1395, Despot Stephen Lazaravić – until 1425) were bordering with two Bulgarian domains (Bulgarian Empire and Principality of Vidin – until 1393 and 1396 respectively). The Ottoman conquest was conducted in several stages between 1385 and 1425.  

The victorious Turks organized the region in a way that partially reflects the previous political conditions. Hence, the Visoka and neighboring region of Znepolje had the status of individual Nahiye. They were part of the Sofia Sandjak. More to the north, the Danube city of Vidin was the center of the Sandjak of Vidin[^1]. The digital map reflects this administrative division through the introduction of three distinct layers within the map.

[^1]: Rossitsa Gradeva, Administrative system and provincial government in the Central Balkan territories of the Ottoman empire, 15th century, H. C. Güzel, C. C. Oğuz, and O. Karatay (eds), The Turks, vol. 3, Ottomans, Ankara 2003, 498-507.

Observing the situation on the micro-scale, one should know that the late medieval and early Ottoman villages were the basic territorial and administrative units with well-established external parameters and rudimentary self-management. Even the deserted kept their specific legal status and economic value since they could be easier resettled and agricultural terrain within its territorial domain utilized externally[^2].

[^2]: Siniša Mišić: Srpsko selo u srednjem veku, (Belgrade: 2019).

### Environmental history: 

Multiple studies have shown the significant impact the natural environment played on the economics and everyday life of the rural communities in southeast Europe during the late middle ages and early Ottoman period. In this case, enclosing mountain pick and ridges made Visoka relatively isolated, creating a distinct small-size administrative unite while the highland terrain provided limited economical resources. Hence, the suggested case study may serve as a solid base for the introduction with the principal elements of the environmental history, which forms an inseparable part of the human heritage.  

### Geographical aspects:

As specified earlier, observing the characteristics of the local terrain, it is possible to establish the natural border areas of Visoka, such as high mountain ranges, that contributed to its geographical separation. The same applies to the character of the settlement’s distribution or the communication routes that served as a capital or secondary connector to the outworld. In this term, the production of a 3D model of the Visoka region may play a crucial role both for geographical and socio-historical interpretation. To include the comparative method in our MOOC, the statistical analysis of the elevation data for the settlements which made the Visoka region and those belonging to the neighboring administrative units will be made.

## Aims 

A. Creation of a digital map which will contain three layers: 
  1. All villages belonging to the Visoka region 
  2. villages of the Znepolje region that are bordering the Visoka 
  3. villages of the Vidin Sandzak that are bordering the Visoka
  
B. Methodological and Didactic aspects:

Creation of a research question and digital products that may help to resolve them, and test the suggested model. In this case, the position of settlements should indicate the geographical limits of the Visoka region and help us discuss the natural and human-made factors which crucially contributed to this situation.

## Skills and level of Competences

### Level of Competence: 

Basic to advanced

### Skills to be learned: 

- extracting geodata from written historical sources; 
- extracting geodata (longitude, latitude, elevation); 
- modeling data; 
- creation of digital maps with different layers; 
- creation of a 3D terrain model 
- interpreting digital maps 

## Workflow

{{<mermaid>}}
flowchart TB
    subgraph Raster
    direction TB
    A[Online Raster Provider] -->|Getting a DEM!| B[DEM]
    B --> |A smaller Raster| C[Local and smaller DEM]
    end
    subgraph Vector
    direction TB
    D[KML] -->|Moving from KML to GPKG!| E[GPKG]
    end
    Raster -->F[3D View]
    Vector -->F
click A "https://oleroy.gitpages.huma-num.fr/minerva/project/dl_dem/"
click B "https://oleroy.gitpages.huma-num.fr/minerva/project/prepare_dem/"
click D "https://oleroy.gitpages.huma-num.fr/minerva/project/kml_to_gpkg/"
click F "https://oleroy.gitpages.huma-num.fr/minerva/project/3d_view/"
{{</mermaid>}}


### Dictionary of less-known terms and expressions

*Sanjak* - An administrative region under the Ottoman Empire, a subdivision of a Vilayet. Sandjak was consistent with several nahiye.

*Nahiye* (alternatively - Nahia) is a regional or local type of territorial/administrative division that usually consists of several villages or sometimes smaller towns.
