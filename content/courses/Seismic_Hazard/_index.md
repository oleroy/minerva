---
date: "2021-06-10"
draft: false
lastmod: "`r Sys.time()`"
linktitle: An evaluation of cultural heritage sites according to the map of Seismic Hazard Design ground acceleration, Slovenia
summary: Considering seismic hazard as an important part of cultural heritage preservation 
title: An evaluation of cultural heritage sites according to the map of Seismic Hazard, Design ground acceleration, Slovenia
toc: true
type: docs
tags:
- Seismic Hazard
menu: Seismic_Hazard
---

## Introduction

**Why do we have to consider seismic hazard as an important part of cultural heritage preservation?**

Earthquakes affect our everyday lives and when they occur, they often cause casualties and damage on infrastructure, including cultural heritage sites. There have been many earthquakes in the past. One of the most known earthquakes took place in 1895 in Slovenia, when an earthquake damaged the capital city of Ljubljana. The Ljubljana basin is one of the most endangered areas according to the seismic activity. Another endangered area is NW Slovenia. In Breginj area (W Slovenia), there were many earthquakes in the past, including the one in 1976 (known as Friuli earthquake). Also in other areas (e.g. L’Aquila, Italy; Petrinja, Croatia) earthquakes made a lot of damage. See the links below for further information on how these natural hazards affected settlements and societies, and of course – cultural heritage.

### Learn more about earthquake:

* The case of Friuli earthquake in 1976 (with description for Breginj area, W Slovenia):
    - [URL 1](https://en.wikipedia.org/wiki/1976_Friuli_earthquake) 
    - [URL 2](https://breginjskikoten.wordpress.com/breginjski-kot/history_culture/old_village_centre_-breginj/)

* The case of Ljubljana earthquake in 1895 
    - [URL](https://en.wikipedia.org/wiki/1895_Ljubljana_earthquake)

* The case of L’Aquila Earthquake in 2009:
    - [URL 1](https://en.wikipedia.org/wiki/2009_L%27Aquila_earthquake)
    - [URL 2](https://cyark.org/news/cultural-heritage-and-identity-in-the-wake-of-the-laquila-earthquake)

* The case of Petrinja earthquake in 2020 (with detailed description of cultural heritage damage):
    - [URL 1](https://en.wikipedia.org/wiki/2020_Petrinja_earthquake) 
    - [URL 2](https://www.iiconservation.org/content/earthquake-damage-cultural-heritage-central-croatia)

The topic of seismic hazard and cultural heritage has special attention in scientific publishing. In 2020, a special issue of Journal of Seismology (volume 24, issue 4) was dedicated to Cultural Heritage and Earthquakes (see [URL](https://link.springer.com/journal/10950/volumes-and-issues/24-4)).
Therefore, researchers include seismic hazard to multi-risk analysis. For a wide European example, see one of the latest studies of Valagussa et al. (2021)[^1].

[^1]:Valagussa, A., Frattini, P., Crosta, G. et al. Multi-risk analysis on European cultural and natural UNESCO heritage sites. Nat Hazards 105, 2659–2676 (2021) [URL](https://doi.org/10.1007/s11069-020-04417-7)

There are also other hazards that threaten to cultural heritage. [Here](https://www.interreg-central.eu/Content.Node/ProteCHt2save.html) you can see an example of EU ProteCHt2save project dealing with risk assessment and sustainable protection of cultural heritage in general.

An interesting [video](https://www.youtube.com/watch?v=MQNKpS0xrwM) about Earthquake Hazards vs Earthquake Risks (IRIS Earthquake Hazard). 

## Aims

The aim of the case study is to collect data on cultural heritage sites in Slovenia, extract/select sites classified as buildings, and intersect them with a map of Seismic Hazard (Design ground acceleration) in order to assess the most endangered locations of buildings listed in the Register of Slovene cultural heritage.

## Skills and level of Competences

### Level of Competence: 

Basic to advanced

### Skills to be learned: 

For completing the task the student will have to acquire the following skills:
    
  - Managing datasets: downloading and opening of datasets.
  - Querying: selection of CH units that are marked as ‘buildings’
  - Spatial analysis (with vector data): intersection of a data layers (data on seismic hazard and data on CH register)
  - Presenting results: preparation of table with a list of CH units and related seismic hazard, production of a simple map

## Data

### The map of Seismic Hazard – Design ground acceleration

Description of data (according to Šket Motnikar and Zupančič 2016[^2]): Seismic hazard is assigned according to the probability of the occurrence of earthquakes. It is estimated based on past earthquakes and on seismotectonic knowledge. The best prevention against earthquakes is seismic-resistant building in accordance with regulations and seismic hazard maps. The Design Ground Acceleration Map refers to a 475-year return period and rock ground type (EC8 ground type A). EC8 soil factors should be applied for other ground types. The return period is the average time between two earthquakes that cause exceeding of the given value at a given location. The return period of 475 years corresponds to a 10% probability of exceedance over 50 years, which is presumed to be the ordinary lifetime of a building. Design ground acceleration values are arranged in zones and rounded up to their upper value.

[^2]: Šket Motnikar, B., Zupančič, P. 2016: Source Seismic hazard – Design ground acceleration. In: Novak M., Rman, N. (eds) Geological Atlas of Slovenia/Geološki atlas Slovenije, p 98.

Data available at: Slovenian Environment Agency, Ministry of the Environment and Spatial Planning, Republic of Slovenia

Links:
- See data viewer at this [web page](http://gis.arso.gov.si/atlasokolja/profile.aspx?id=Atlas_Okolja_AXL@Arso) 
- Data available at this [web page](https://gis.arso.gov.si/wfs_web/faces/WFSLayersList.jspx)

### Register of immovable cultural heritage – selection of locations of immovable cultural heritage

Description of data: The Register of immovable cultural heritage (si. Register nepremične kulturne dediščine) is an official collection of data regarding immovable cultural heritage for the area of the Republic of Slovenia. Each unit of the register has its ID number (EŠD). The units are classified in different types (buildings, archaeological sites, parks and gardens etc.). Please, note that cultural heritage data, freely available at OPSI portal[^3] has a selection of units.

[^3]: Odprti podatki Slovenije (OPSI). URL: https://podatki.gov.si/dataset/izbor-iz-registra-nepremicne-kulturne-dediscine (Accessed: 28 May 2021)

Data available at: Ministry of culture, Republic of Slovenia

Links:
- [Data viewer](https://gisportal.gov.si/portal/apps/webappviewer/index.html?id=df5b0c8a300145fda417eda6b0c2b52b)
- [Data available](https://podatki.gov.si/dataset/izbor-iz-registra-nepremicne-kulturne-dediscine)

## Slovenian municipalities

Description of data: Slovenia is formally divided in 212 municipalities[^4].

[^4]: Geodetska uprava Republike Slovenije, Register of Spatial Units, 28. 5. 2021/Surveying and Mapping Authority, Republic of Slovenia, Register of spatial units, 28. 5. 2021. 

Data available at: Surveying and Mapping Authority, Republic of Slovenia:

  - [Links](https://egp.gu.gov.si/egp/?lang=en) (Registration required!)

## Workflow:

{{<mermaid>}}
graph TD
  A[QGIS Project]--> B[Adding Data]
  B--> C[Exploring data, Quick Visualization]
  C--> D[Filtering Data of Interest]
  D--> E[Spatial Join]
  E--> F[Print Layout]
  click C "https://oleroy.gitpages.huma-num.fr/minerva/project/explo_quick_viz/"
  click D "https://oleroy.gitpages.huma-num.fr/minerva/project/selection_filter/"
  click E "https://oleroy.gitpages.huma-num.fr/minerva/project/spatial_join/"
{{</mermaid>}}


**DISCLAMER:** The data is freely available at the website of ARSO and MK. The data used in this tutorial and results presented are only intended for simulation of GIS tool usage. There is absolutely no guarantee that data are valid and have high accuracy.











