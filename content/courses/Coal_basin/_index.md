---
date: "2021-05-19T00:00:00Z"
draft: false
lastmod: "`r Sys.time()`"
linktitle: Coal Basin in Saint-Etienne, France
summary: From an 1846 historical map showing location of different coal mining concessions with several stored in the legend (as a table) in the region of Saint-Etienne, we want to create a shape for calculate some basic statistics  
title: Overview
toc: true
type: docs
tags:
- Coal Basin Saint-Etienne
menu: Coal_basin
---

## Aims:

From an 1846 historical map showing location of different coal mining concessions with several stored in the legend (as a table) in the region of Saint-Etienne, we want to create a shape for calculate some basic statistics. 


## Skills.

### Level of Competence: 

Basic to advanced.

### Skills to be learned:

- [Archive data acquisition](https://oleroy.gitpages.huma-num.fr/minerva/project/dl_image_gallica/)
- [Géororeferencing](https://oleroy.gitpages.huma-num.fr/minerva/project/georeferencing/) a map
- CRS management
    * [Reprojecting a raster](https://oleroy.gitpages.huma-num.fr/minerva/project/reprojecting/)
- Digitizing data:
    * [Creating new shapefile and Snapping Options](https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing/)
    * [Creating and modifying](https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing-partb/) features 
- [Attribute Table](https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing-partc/)
- Using a Web Map Service

## Workflow

{{<mermaid>}}
graph TD
  A[Online library]-->|Downloading data from Gallica's web site| B[Image]
  B-->|Georeferecing an image| C[Georefereced image]
  C-->|Reprojecting a raster| D[Reprojected image]
  D -->|Digitizing| E[Shapefile]
  D -->|Adding attributes| E 
  click A "https://oleroy.gitpages.huma-num.fr/minerva/project/dl_image_gallica/"
  click B "https://oleroy.gitpages.huma-num.fr/minerva/project/georeferencing/"
  click C "https://oleroy.gitpages.huma-num.fr/minerva/project/reprojecting/"
  click D "https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing/"
  click E "https://oleroy.gitpages.huma-num.fr/minerva/project/digitizing-partc/"
{{</mermaid>}}


## Data Needed:

- An [historical map](https://gallica.bnf.fr/ark:/12148/btv1b53085116w.r=saint-etienne?rk=257512;0&lang=EN): Carte du bassin houiller de Saint-Etienne et de Rive-de-Gier (1846)
- Reference map: OSM Web Map Service
